import { Injectable } from '@nestjs/common';
import data from './mock-data/products.json';
import { ProductModel } from '@power-shop/model-data';

@Injectable()
export class AppService {
  getData(): ProductModel[] {
    return data as ProductModel[];
  }
}
