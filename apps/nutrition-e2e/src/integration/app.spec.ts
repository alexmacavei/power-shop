import { getGreeting } from '../support/app.po';

describe('nutrition', () => {
  beforeEach(() => cy.visit('/'));

  it('should display welcome message', () => {
    getGreeting().contains('Welcome to nutrition!');
  });
});
