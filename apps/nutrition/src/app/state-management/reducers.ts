import { CartActionType, ProductsActionType } from './actions';
import { CartState, ItemInCart, ProductState } from './state';
import produce from 'immer';

const initialProductState: ProductState = {
  products: [],
  isFetching: false,
  lastUpdated: new Date(),
  errorFetching: undefined
};

const initialCartState: CartState = {
  itemsInCart: [],
  totalPrice: 0
};

export function productReducer(state: ProductState = initialProductState, action: ProductsActionType): ProductState {
  switch (action.type) {
    case 'RECEIVE_PRODUCTS':
      return produce(state, draftState => {
        draftState.isFetching = false;
        draftState.lastUpdated = new Date();
        draftState.products = action.payload;
        draftState.errorFetching = undefined;
      });
    case 'REQUEST_PRODUCTS':
      return produce(state, draftState => {
        draftState.isFetching = true;
        draftState.errorFetching = undefined;
      });
    case 'RECEIVE_PRODUCTS_ERROR':
      return produce(state, draftState => {
        draftState.products = [];
        draftState.isFetching = false;
        draftState.errorFetching = action.payload;
      });
    default:
      return { ...state };
  }
}

function calculateTotalPriceInCart(items: ItemInCart[]): number {
  return items.reduce((sum: number, crtElem: ItemInCart) => sum + crtElem.item.price * crtElem.numberOfItemsInCart, 0);
}

export function cartReducer(state: CartState = initialCartState, action: CartActionType): CartState {
  switch (action.type) {
    case 'ADD_PRODUCT_TO_CART':
      return produce(state, draftState => {
        const foundItem: ItemInCart = draftState.itemsInCart.find(i => i.item.id === action.payload.id);
        if (foundItem) {
          foundItem.numberOfItemsInCart += 1;
        } else {
          draftState.itemsInCart.push({
            item: action.payload,
            numberOfItemsInCart: 1
          });
        }
        draftState.totalPrice += action.payload.price;
      });
    case 'REMOVE_ENTIRE_PRODUCT_FROM_CART':
      return produce(state, draftState => {
        const indexToRemove = draftState.itemsInCart.findIndex(i => i.item.id === action.payload);
        draftState.itemsInCart.splice(indexToRemove, 1);
        draftState.totalPrice = calculateTotalPriceInCart(draftState.itemsInCart);
      });
    case 'REMOVE_PRODUCT_FROM_CART':
      return produce(state, draftState => {
        const foundItemIndex = draftState.itemsInCart.findIndex(i => i.item.id === action.payload);
        const actualFoundItem = draftState.itemsInCart[foundItemIndex];
        if (foundItemIndex !== -1) {
          draftState.totalPrice -= actualFoundItem.item.price;
          if (actualFoundItem.numberOfItemsInCart === 1) {
            draftState.itemsInCart.splice(foundItemIndex, 1);
          } else {
            actualFoundItem.numberOfItemsInCart -= 1;
          }
        }
      });
    default:
      return { ...state };
  }
}
