import { ProductModel } from '@power-shop/model-data';
import { environment } from '../../environments/environment';

export type REQUEST_PRODUCTS = 'REQUEST_PRODUCTS';
export type RECEIVE_PRODUCTS = 'RECEIVE_PRODUCTS';
export type ADD_PRODUCT_TO_CART = 'ADD_PRODUCT_TO_CART';
export type REMOVE_PRODUCT_FROM_CART = 'REMOVE_PRODUCT_FROM_CART';
export type REMOVE_ENTIRE_PRODUCT_FROM_CART = 'REMOVE_ENTIRE_PRODUCT_FROM_CART';
export type RECEIVE_PRODUCTS_ERROR = 'RECEIVE_PRODUCTS_ERROR';

export interface ActionType {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  payload: any;
}

export interface ProductsActionType extends ActionType {
  type: REQUEST_PRODUCTS | RECEIVE_PRODUCTS | RECEIVE_PRODUCTS_ERROR;
}

export interface CartActionType extends ActionType {
  type: ADD_PRODUCT_TO_CART | REMOVE_PRODUCT_FROM_CART | REMOVE_ENTIRE_PRODUCT_FROM_CART;
}

export function requestProducts(): ProductsActionType {
  return {
    type: 'REQUEST_PRODUCTS',
    payload: undefined
  };
}

export function receiveProducts(apiResult): ProductsActionType {
  return {
    type: 'RECEIVE_PRODUCTS',
    payload: apiResult
  };
}

export function receiveErrorFromProductsApi(error): ProductsActionType {
  return {
    type: 'RECEIVE_PRODUCTS_ERROR',
    payload: error
  };
}

export function fetchProducts() {
  const productsApiUrl = `${environment.apiUrl}/api`;

  return function(dispatch) {
    dispatch(requestProducts());
    return fetch(productsApiUrl)
      .then(response => response.json(), error => dispatch(receiveErrorFromProductsApi(error)))
      .then(json => dispatch(receiveProducts(json)));
  };
}

export function addProductToCart(product: ProductModel): CartActionType {
  return {
    type: 'ADD_PRODUCT_TO_CART',
    payload: product
  };
}

export function removeEntireProductFromCart(productId: number): CartActionType {
  return {
    type: 'REMOVE_ENTIRE_PRODUCT_FROM_CART',
    payload: productId
  };
}

export function decreaseProductQuantityInCart(productId: number): CartActionType {
  return {
    type: 'REMOVE_PRODUCT_FROM_CART',
    payload: productId
  };
}
