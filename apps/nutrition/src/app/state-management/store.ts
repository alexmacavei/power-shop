import { applyMiddleware, combineReducers, createStore } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { cartReducer, productReducer } from './reducers';
import { fetchProducts } from './actions';
import { composeWithDevTools } from 'redux-devtools-extension';

const rootReducer = combineReducers({ products: productReducer, cart: cartReducer });
export type AppState = ReturnType<typeof rootReducer>;

const composeEnhancers = composeWithDevTools({ trace: true });

export const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunkMiddleware)));

// @ts-ignore
store.dispatch(fetchProducts());
