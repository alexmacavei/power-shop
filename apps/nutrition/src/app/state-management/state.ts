import { ProductModel } from '@power-shop/model-data';

export interface ItemInCart {
  item: ProductModel;
  numberOfItemsInCart: number;
}

export interface CartState {
  itemsInCart: ItemInCart[];
  totalPrice: number;
}

export interface ProductState {
  products: ProductModel[];
  isFetching: boolean;
  lastUpdated: Date;
  errorFetching: string | undefined;
}
