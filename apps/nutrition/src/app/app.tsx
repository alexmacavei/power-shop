import React from 'react';

import './app.scss';
import Navbar from './components/navbar.component';
import { BrowserRouter } from 'react-router-dom';
import { ProductList } from './components/product-list.component';
import ProductDetails from './components/product-details.component';
import ShoppingCart from './components/cart.component';
import EmptyDefaultPage from './components/empty-default-page.component';
import { Switch, Route } from 'react-router-dom';

export const App = () => {
  return (
    <BrowserRouter>
      <Navbar />
      <Switch>
        <Route exact path='/' component={ProductList} />
        <Route path='/details/:id' component={ProductDetails} />
        <Route path='/cart' component={ShoppingCart} />
        <Route component={EmptyDefaultPage} />
      </Switch>
    </BrowserRouter>
  );
};

export default App;
