import React from 'react';
import {
  Button,
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CardMedia,
  makeStyles,
  Typography
} from '@material-ui/core';
import { ProductModel } from '@power-shop/model-data';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { addProductToCart } from '../state-management/actions';
import { AppState } from '../state-management/store';
import { ItemInCart } from '../state-management/state';

const useStyles = makeStyles({
  media: {
    height: 200,
    objectFit: 'contain',
    width: 200,
    margin: '0 auto'
  },
  cardBody: {
    width: 345
  },
  productTitle: {
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis'
  },
  link: {
    textDecoration: 'none',
    color: '#000000'
  }
});

export const Product = (props: { forModel: ProductModel }) => {
  const classes = useStyles();
  const itemsInCart: ItemInCart[] = useSelector((state: AppState) => state.cart.itemsInCart);
  const dispatch = useDispatch();

  function isItemInCart(item: ProductModel): boolean {
    return !!itemsInCart.find(i => i.item.id === item.id);
  }

  return (
    <Card className={classes.cardBody}>
      <CardActionArea>
        <Link to={'/details/' + props.forModel.id} className={classes.link}>
          <CardMedia className={classes.media} image={props.forModel.imageUrl} title={props.forModel.name} />
          <CardContent>
            <Typography className={classes.productTitle} gutterBottom variant='body1' component='h2'>
              {props.forModel.name}
            </Typography>
          </CardContent>
        </Link>
      </CardActionArea>
      <CardActions>
        <Button
          variant='contained'
          size='small'
          color='primary'
          disabled={isItemInCart(props.forModel)}
          onClick={() => dispatch(addProductToCart(props.forModel))}>
          {isItemInCart(props.forModel) ? 'Deja adăugat' : 'Adaugă în coș'}
        </Button>
      </CardActions>
    </Card>
  );
};

export default Product;
