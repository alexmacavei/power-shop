import React, { SyntheticEvent, useEffect } from 'react';
import Product from './product.component';
import {
  CircularProgress,
  Container,
  createStyles,
  Grid,
  IconButton,
  makeStyles,
  Snackbar,
  SnackbarContent,
  Theme
} from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from '../state-management/store';
import { ProductState } from '../state-management/state';
import ErrorIcon from '@material-ui/icons/Error';
import CloseIcon from '@material-ui/icons/Close';
import { fetchProducts } from '../state-management/actions';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      backgroundColor: '#f5f5f5',
      paddingTop: 30
    },
    card: {
      flexGrow: 0
    },
    progress: {
      margin: theme.spacing(2)
    },
    error: {
      backgroundColor: theme.palette.error.dark
    },
    message: {
      display: 'flex',
      alignItems: 'center'
    },
    iconVariant: {
      opacity: 0.9,
      marginRight: theme.spacing(1)
    },
    icon: {
      fontSize: 20
    }
  })
);

export const ProductList = () => {
  const classes = useStyles();
  const productsFetchState: ProductState = useSelector((state: AppState) => state.products);
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchProducts());
  }, []);

  function handleCloseSnackbar(event?: SyntheticEvent, reason?: string) {
    if (reason === 'clickaway') {
      return;
    }

    setOpenSnackbar(false);
  }

  function displayErrorSnackbar() {
    setOpenSnackbar(true);
    return (
      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center'
        }}
        open={openSnackbar}
        autoHideDuration={6000}
        onClose={handleCloseSnackbar}>
        <MySnackbarContentWrapper
          onClose={handleCloseSnackbar()}
          variant='error'
          message='Eroare la aducerea informațiilor despre produse de la server!'
        />
      </Snackbar>
    );
  }

  const displayProductsOrError = productsFetchState.errorFetching
    ? displayErrorSnackbar()
    : productsFetchState.products.map(p => (
        <Grid key={p.name} item className={classes.card}>
          <Product forModel={p} />
        </Grid>
      ));

  return (
    <div className={classes.root}>
      <Container>
        <Grid container spacing={2}>
          {productsFetchState.isFetching ? <CircularProgress className={classes.progress} /> : displayProductsOrError}
        </Grid>
      </Container>
    </div>
  );
};

const MySnackbarContentWrapper = props => {
  const classes = useStyles();
  const { className, message, onClose, variant, ...other } = props;

  return (
    <SnackbarContent
      className={classes.error}
      aria-describedby='client-snackbar'
      message={
        <span id='client-snackbar' className={classes.message}>
          <ErrorIcon className={classes.iconVariant} />
          {message}
        </span>
      }
      action={[
        <IconButton key='close' aria-label='close' color='inherit' onClick={onClose}>
          <CloseIcon className={classes.icon} />
        </IconButton>
      ]}
      {...other}
    />
  );
};
