import React from 'react';
import {
  Button,
  ButtonBase,
  ButtonGroup,
  CircularProgress,
  createStyles,
  Grid,
  Input,
  makeStyles,
  Paper,
  Theme,
  Typography
} from '@material-ui/core';
import { useSelector } from 'react-redux';
import { AppState } from '../state-management/store';
import { ProductState } from '../state-management/state';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      backgroundColor: '#f5f5f5'
    },
    paper: {
      padding: theme.spacing(3, 2),
      margin: '1rem',
      minHeight: '60vh'
    },
    image: {
      width: 400
    },
    img: {
      margin: 'auto',
      display: 'block',
      maxWidth: '100%',
      maxHeight: '100%'
    },
    inputIncrementer: {
      border: '1px #0000003b solid',
      width: '50px',
      background: '#f5f5f5',
      borderLeftColor: '#0000003b !important'
    }
  })
);

const ProductDetails = ({ match }) => {
  const classes = useStyles();
  const productState: ProductState = useSelector((state: AppState) => state.products);

  function buildItemDetails() {
    const productWithDetails = productState.products.find(p => p.id.toString() === match.params.id);
    return (
      <Paper className={classes.paper}>
        <Grid container spacing={2}>
          <Grid item>
            <ButtonBase className={classes.image}>
              <img className={classes.img} alt='complex' src={productWithDetails.imageUrl} />
            </ButtonBase>
          </Grid>
          <Grid item xs={12} sm container>
            <Grid item xs container direction='column' spacing={2}>
              <Grid item xs>
                <Typography gutterBottom variant='h4'>
                  {productWithDetails.name}
                </Typography>

                <Typography variant='h5' style={{ color: 'green' }}>{`${
                  productWithDetails.price
                } ${productWithDetails.currency}`}</Typography>
                <br />
                <Typography variant='body2' gutterBottom>
                  {productWithDetails.description}
                </Typography>
                <Typography variant='body2' color='textSecondary'>
                  {`Produse în stoc: ${productWithDetails.itemsInStock}`}
                </Typography>
              </Grid>
              <Grid item>
                <ButtonGroup size='small' aria-label='small outlined button group'>
                  <Button>+</Button>
                  <Input className={classes.inputIncrementer}>Two</Input>
                  <Button>-</Button>
                </ButtonGroup>&nbsp;
                <Button variant='contained' color='primary'>
                  <AddCircleOutlineIcon style={{ paddingRight: '5px' }} />
                  Adaugă în coș
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Paper>
    );
  }

  return (
    <div className={classes.root}>
      <Grid container direction='column' alignItems={'center'} style={{ minHeight: '100vh' }}>
        <Grid item xs={12} lg={8} md={8} style={{ height: '100%' }}>
          {!productState.isFetching ? buildItemDetails() : <CircularProgress />}
        </Grid>
      </Grid>
    </div>
  );
};

export default ProductDetails;
