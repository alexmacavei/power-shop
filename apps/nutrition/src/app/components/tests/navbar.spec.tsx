import React from 'react';
import { cleanup } from '@testing-library/react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Navbar from '../navbar.component';
import BrowserRouter from 'react-router-dom';

describe('Navbar', () => {
  afterEach(cleanup);

  configure({ adapter: new Adapter() });

  it('should render successfully', () => {
    const baseElement = shallow(<Navbar />);

    expect(baseElement).toBeTruthy();
  });
});
