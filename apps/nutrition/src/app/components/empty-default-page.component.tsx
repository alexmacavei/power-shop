import React from 'react';
import { Typography } from '@material-ui/core';

const EmptyDefaultPage = () => {
  return (
    <div>
      <Typography>Error, page not found et al!</Typography>
    </div>
  );
};

export default EmptyDefaultPage;
