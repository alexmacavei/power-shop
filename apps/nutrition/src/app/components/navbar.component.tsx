import React from 'react';
import {
  AppBar,
  Badge,
  createStyles,
  IconButton,
  InputBase,
  makeStyles,
  Menu,
  MenuItem,
  Theme,
  Toolbar,
  Tooltip,
  Typography
} from '@material-ui/core';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import CategoryIcon from '@material-ui/icons/Category';
import SearchIcon from '@material-ui/icons/Search';
import FitnessCenterIcon from '@material-ui/icons/FitnessCenter';
import { Link } from 'react-router-dom';
import { fade } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';
import { AppState } from '../state-management/store';
import { ItemInCart } from '../state-management/state';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1
    },
    menuButton: {
      marginRight: theme.spacing(2)
    },
    title: {
      marginLeft: '10px',
      flexGrow: 1
    },
    link: {
      textDecoration: 'none',
      color: '#FFFFFF'
    },
    cart: {
      color: '#FFFFFF',
      alignItems: 'right'
    },
    search: {
      position: 'relative',
      borderRadius: theme.shape.borderRadius,
      backgroundColor: fade(theme.palette.common.white, 0.15),
      '&:hover': {
        backgroundColor: fade(theme.palette.common.white, 0.25)
      },
      marginRight: theme.spacing(2),
      marginLeft: 0,
      width: '100%',
      [theme.breakpoints.up('sm')]: {
        marginLeft: theme.spacing(3),
        width: 'auto'
      }
    },
    searchIcon: {
      width: theme.spacing(7),
      height: '100%',
      position: 'absolute',
      pointerEvents: 'none',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center'
    },
    inputRoot: {
      color: 'inherit'
    },
    inputInput: {
      padding: theme.spacing(1, 1, 1, 7),
      transition: theme.transitions.create('width'),
      width: '100%',
      [theme.breakpoints.up('md')]: {
        width: 200
      }
    },
    badgeMargin: {
      margin: theme.spacing(2)
    },
    categories: {
      color: '#FFFFFF',
      fontSize: 16
    },
    shoppingCartButton: {
      padding: 0
    }
  })
);

const Navbar = () => {
  const classes = useStyles();
  const itemsInCart: ItemInCart[] = useSelector((state: AppState) => state.cart.itemsInCart);
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  function handleClick(event: React.MouseEvent<HTMLButtonElement>) {
    setAnchorEl(event.currentTarget);
  }

  function handleClose() {
    setAnchorEl(null);
  }

  function computeAllItemsInCart(): number {
    return itemsInCart.map(i => i.numberOfItemsInCart).reduce((sum, crtElem) => sum + crtElem, 0);
  }

  return (
    <div className={classes.root}>
      <AppBar position='static'>
        <Toolbar>
          <FitnessCenterIcon />
          <Link to='/' className={classes.link}>
            <Typography variant='h6' className={classes.title}>
              Magazin Fitness
            </Typography>
          </Link>

          <div className={classes.root} />
          <IconButton
            className={classes.categories}
            aria-controls='simple-menu'
            aria-haspopup='true'
            onClick={handleClick}>
            <CategoryIcon />
            Categorii
          </IconButton>
          <Menu id='simple-menu' anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={handleClose}>
            <MenuItem onClick={handleClose}>Chestii</MenuItem>
            <MenuItem onClick={handleClose}>Pastile</MenuItem>
            <MenuItem onClick={handleClose}>Licori</MenuItem>
          </Menu>

          <div className={classes.search}>
            <div className={classes.searchIcon}>
              <SearchIcon />
            </div>
            <InputBase
              placeholder='Caută…'
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput
              }}
              inputProps={{ 'aria-label': 'search' }}
            />
          </div>

          <Tooltip title={'View cart'}>
            <Link to='/cart' className={classes.cart}>
              <IconButton color='inherit' className={classes.shoppingCartButton}>
                <Badge className={classes.badgeMargin} badgeContent={computeAllItemsInCart()} color='secondary'>
                  <ShoppingCartIcon />
                </Badge>
              </IconButton>
            </Link>
          </Tooltip>
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default Navbar;
