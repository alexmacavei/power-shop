import React from 'react';
import { cleanup, render } from '@testing-library/react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import App from './app';

describe('App', () => {
  afterEach(cleanup);

  configure({ adapter: new Adapter() });

  it('should render successfully', () => {
    const { baseElement } = render(<App />);

    expect(baseElement).toBeTruthy();
  });

  it('should contain a Navbar and a Switch under the BrowserRouter', () => {
    const component = shallow(<App />);
    const childComponents = component.getElements()[0].props.children;
    expect(childComponents.length).toEqual(2);
    expect(childComponents[0].type.name).toEqual('Navbar');
    expect(childComponents[1].type.name).toEqual('Switch');
  });

  it('should contain a route for default home "/" path', () => {
    const [childComponents] = shallow(<App />);
    const routes = childComponents.props.children[1].props.children;
    expect(routes[0].props.exact).toBeTruthy();
    expect(routes[0].props.path).toEqual('/');
    expect(routes[0].props.component).toBeTruthy();
  });
});
