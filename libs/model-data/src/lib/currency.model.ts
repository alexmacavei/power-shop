export enum Currency {
  EUR = 'EUR',
  RON = 'RON',
  USD = 'USD',
  GBP = 'GBP'
}
