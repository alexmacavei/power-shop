import { Currency } from './currency.model';

export interface ProductModel {
  id: number;
  name: string;
  imageUrl: string;
  price: number;
  currency: Currency;
  itemsInStock: number;
  description: string;
}
